module Spec.Y2015.D02
  ( runSuite
  )
where

import           Test.Hspec

import           Spec.Hash                      ( hash )

import           Aoc.Y2015.D02                  ( Box(..)
                                                , parse
                                                , part1
                                                , part2
                                                , doPart1
                                                , doPart2
                                                )

runSuite :: SpecWith ()
runSuite = describe "02" $ do
  describe "solutions" $ do
    it "1" $ hash part1 `shouldBe` 62679985975660910
    it "2" $ hash part2 `shouldBe` 62679985977635593

  describe "parsing" $ do
    testHelper "2x3x4"  parse [Box 2 3 4]
    testHelper "1x1x10" parse [Box 1 1 10]

  describe "examples" $ do
    describe "part 1" $ do
      testHelper "2x3x4"  (doPart1 . parse) 58
      testHelper "1x1x10" (doPart1 . parse) 43

    describe "part 2" $ do
      testHelper "2x3x4"  (doPart2 . parse) 34
      testHelper "1x1x10" (doPart2 . parse) 14

testHelper :: (Show a, Eq a) => String -> (String -> a) -> a -> SpecWith ()
testHelper lbl f e = it lbl $ f lbl `shouldBe` e
