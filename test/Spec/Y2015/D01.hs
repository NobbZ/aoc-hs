module Spec.Y2015.D01
  ( runSuite
  )
where

import           Test.Hspec

import           Spec.Hash                      ( hash )

import           Aoc.Y2015.D01                  ( Paren(..)
                                                , parse
                                                , part1
                                                , part2
                                                , doPart1
                                                , doPart2
                                                )

runSuite :: SpecWith ()
runSuite = describe "01" $ do
  describe "solutions" $ do
    it "1" $ hash part1 `shouldBe` 62679985974121173
    it "2" $ hash part2 `shouldBe` 62679985974121674

  describe "parsing" $ do
    it "(())" $ parse "(())" `shouldBe` [Open, Open, Close, Close]
    it "()()" $ parse "()()" `shouldBe` [Open, Close, Open, Close]
    it "(((" $ parse "(((" `shouldBe` [Open, Open, Open]
    it "(()(()("
      $          parse "(()(()("
      `shouldBe` [Open, Open, Close, Open, Open, Close, Open]
    it "))((((("
      $          parse "))((((("
      `shouldBe` [Close, Close, Open, Open, Open, Open, Open]
    it "())" $ parse "())" `shouldBe` [Open, Close, Close]
    it "))(" $ parse "))(" `shouldBe` [Close, Close, Open]
    it ")))" $ parse ")))" `shouldBe` [Close, Close, Close]
    it ")())())"
      $          parse ")())())"
      `shouldBe` [Close, Open, Close, Close, Open, Close, Close]

  describe "examples" $ do
    describe "part 1" $ do
      it "(())" $ doPart1 (parse "(())") `shouldBe` 0
      it "()()" $ doPart1 (parse "()()") `shouldBe` 0
      it "(((" $ doPart1 (parse "(((") `shouldBe` 3
      it "(()(()(" $ doPart1 (parse "(()(()(") `shouldBe` 3
      it "))(((((" $ doPart1 (parse "))(((((") `shouldBe` 3
      it "())" $ doPart1 (parse "())") `shouldBe` -1
      it "))(" $ doPart1 (parse "))(") `shouldBe` -1
      it ")))" $ doPart1 (parse ")))") `shouldBe` -3
      it ")())())" $ doPart1 (parse ")())())") `shouldBe` -3

    describe "part2" $ do
      it ")" $ doPart2 (parse ")") `shouldBe` 1
      it "()())" $ doPart2 (parse "()())") `shouldBe` 5
