module Spec.Hash
  ( hash
  )
where

import           Data.Hashable                  ( Hashable
                                                , hashWithSalt
                                                )

hash :: Hashable h => h -> Int
hash = hashWithSalt 0xDEADBEEF
