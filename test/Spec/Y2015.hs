module Spec.Y2015
  ( runTests
  )
where

import           Test.Hspec

import           Spec.Hash                      ( hash )

import qualified Spec.Y2015.D01                as D01
import qualified Spec.Y2015.D02                as D02

import qualified Aoc.Y2015.D03
import qualified Aoc.Y2015.D04

runTests :: SpecWith ()
runTests = describe "2015" $ do
  D01.runSuite
  D02.runSuite

  describe "03" $ do
    it "1" $ hash Aoc.Y2015.D03.part1 `shouldBe` 62679985974122525
    it "2" $ hash Aoc.Y2015.D03.part2 `shouldBe` 62679985974123269

  describe "04" $ do
    it "1" $ hash Aoc.Y2015.D04.part1 `shouldBe` 62679985974395456
    it "2" $ hash Aoc.Y2015.D04.part2 `shouldBe` 62679985967306429
