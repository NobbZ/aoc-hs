import           Test.Hspec

import qualified Spec.Y2015                    as Y2015


main :: IO ()
main = hspec $ do
  Y2015.runTests
