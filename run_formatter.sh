#!/usr/bin/env bash

find {src,test,app} -name '*.hs' -printf '%p\n' -exec brittany --write-mode=inplace \{\} \;

# Install formatter using:
# stack --resolver lts-12.26 install brittany