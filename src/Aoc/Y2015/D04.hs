{-# LANGUAGE TemplateHaskell #-}

module Aoc.Y2015.D04
  ( part1
  , part2
  )
where

import           Aoc.TH.Loader

import           Data.Word
import qualified Crypto.Hash.MD5               as MD5
import qualified Data.Bits                     as Bits
import qualified Data.ByteString               as ByteString
import qualified Data.ByteString.Char8         as BSC
import qualified Data.Char                     as Char
import qualified Data.List                     as List
import qualified Data.Maybe                    as Maybe

$(genBoilerPlateFromLiteral "yzbqklnj")

doPart1 :: String -> Int
doPart1 = solve "00000"


doPart2 :: String -> Int
doPart2 = solve "000000"

solve :: String -> String -> Int
solve p k = Maybe.fromJust
  $ List.findIndex (p `List.isPrefixOf`) [ process n | n <- [0 :: Int ..] ]
 where
  process :: Int -> String
  process n = hexify . ByteString.unpack . MD5.hash $ BSC.pack (k ++ show n)
  hexify :: [Word8] -> String
  hexify = concatMap
    (\b -> map Char.intToDigit
               [fromIntegral b `Bits.shiftR` 4, fromIntegral b Bits..&. 0xf]
    )
