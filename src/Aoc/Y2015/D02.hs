{-# LANGUAGE TemplateHaskell #-}

module Aoc.Y2015.D02
  ( Box(..)
  , parse
  , part1
  , part2
  , doPart1
  , doPart2
  )
where

import           Aoc.TH.Loader

import qualified Data.List                     as List
import qualified Data.String                   as String

data Box = Box Int Int Int deriving (Show, Eq)

$(genBoilerPlate 2015 2)

parse :: String -> [Box]
parse = map parseBox . filter (/= "") . String.lines

parseBox :: String -> Box
parseBox l = let [x, y, z] = map read $ split 'x' l in Box x y z

doPart1 :: [Box] -> Int
doPart1 = sum . map calcWrap

doPart2 :: [Box] -> Int
doPart2 = sum . map calcRibbon

calcWrap :: Box -> Int
calcWrap (Box w l h) =
  let [a, b, c] = List.sort [w * l, w * h, h * l]
  in  let wrap = 2 * (a + b + c) in let slack = a in wrap + slack

calcRibbon :: Box -> Int
calcRibbon (Box w l h) =
  let [a, b, c] = List.sort [w, l, h] in a + a + b + b + a * b * c

split :: Char -> String -> [String]
split c = reverse . map reverse . go [] ""
 where
  go parts curr [] = curr : parts
  go parts curr (x : xs) | c == x    = go (curr : parts) "" xs
                         | otherwise = go parts (x : curr) xs
