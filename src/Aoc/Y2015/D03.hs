{-# LANGUAGE TemplateHaskell #-}

module Aoc.Y2015.D03
  ( part1
  , part2
  )
where

import           Aoc.TH.Loader                  ( genBoilerPlate )

import qualified Data.Set                      as Set
import qualified Data.List                     as List

$(genBoilerPlate 2015 3)

data Direction = Up | Rght | Down | Lft deriving Show

parse :: String -> [Direction]
parse []         = []
parse ('^' : cs) = Up : parse cs
parse ('>' : cs) = Rght : parse cs
parse ('v' : cs) = Down : parse cs
parse ('<' : cs) = Lft : parse cs
parse (c   : _ ) = error $ "Unknown character: " ++ show c

doPart1 :: [Direction] -> Int
doPart1 = Set.size . fst . List.foldl' go (Set.singleton center, center)
  where go (s, p) d = let p' = move p d in (Set.insert p' s, p')

doPart2 :: [Direction] -> Int
doPart2 =
  Set.size
    . fst3
    . List.foldl' go (Set.singleton center, center, center)
    . pairWise
 where
  go (s, ps, pr) (ds, dr) =
    let ps' = move ps ds
        pr' = move pr dr
        s'  = Set.insert ps' $ Set.insert pr' s
    in  (s', ps', pr')
  pairWise (a : b : xs) = (a, b) : pairWise xs
  pairWise []           = []
  pairWise _            = undefined
  fst3 (x, _, _) = x

center :: (Int, Int)
center = (0, 0)

move :: (Int, Int) -> Direction -> (Int, Int)
move (x, y) Up   = (x, y + 1)
move (x, y) Rght = (x + 1, y)
move (x, y) Down = (x, y - 1)
move (x, y) Lft  = (x - 1, y)
