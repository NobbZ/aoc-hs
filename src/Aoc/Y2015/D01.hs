{-# LANGUAGE TemplateHaskell #-}

module Aoc.Y2015.D01
  ( Paren(..)
  , part1
  , part2
  , doPart1
  , doPart2
  , parse
  )
where

import           Aoc.TH.Loader

$(genBoilerPlate 2015 1)

data Paren = Open | Close deriving (Show, Eq)

parse :: String -> [Paren]
parse []         = []
parse ('(' : ps) = Open : parse ps
parse (')' : ps) = Close : parse ps
parse (c   : _ ) = error $ "Unknown character: " ++ show c

doPart1 :: [Paren] -> Int
doPart1 = foldl count 0
 where
  count n Open  = n + 1
  count n Close = n - 1

doPart2 :: [Paren] -> Int
doPart2 = go (0 :: Int) (0 :: Int)
 where
  go idx (-1) _            = idx
  go idx cnt  (Open  : cs) = go (idx + 1) (cnt + 1) cs
  go idx cnt  (Close : cs) = go (idx + 1) (cnt - 1) cs
  go _   _    _            = undefined -- Reaching the end of the input without
                                       -- reaching a basement floor is undefined.
