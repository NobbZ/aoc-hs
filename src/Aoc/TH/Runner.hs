{-# LANGUAGE TemplateHaskell #-}

module Aoc.TH.Runner where

import           Language.Haskell.TH

import           Text.Printf                    ( printf )

genRunAll :: [(Int, [Int])] -> DecsQ
genRunAll solutions = do
  let body = doE $ concatMap mkYear solutions
  [d| runAll :: IO ()
      runAll = $(body) |]

mkYear :: (Int, [Int]) -> [StmtQ]
mkYear (y, ds) = do
  let formatString d p = litE . stringL $ printf "%d %.02d %d: %%d" y d p
  let funCall d p = varE . mkName $ printf "Aoc.Y%d.D%.02d.part%d" y d p
  let parts =
        [ noBindS [| putStrLn $ printf $(formatString d p) $(funCall d p) |]
        | d <- ds
        , p <- [1 :: Int, 2]
        ]
  parts
