{-# LANGUAGE TemplateHaskell #-}

module Aoc.TH.Loader
  ( embedData
  , genBoilerPlate, genBoilerPlateFromLiteral
  )
where

import           Data.FileEmbed                 ( embedStringFile )
import           Language.Haskell.TH
import           Language.Haskell.TH.Syntax     ( liftData )
import           Data.Data                      ( Data )

import           Text.Printf

embedData :: Int -> Int -> Q Exp
embedData y d = embedStringFile $ printf "data/%d/%.02d.txt" y d

genBoilerPlateFromLiteral :: Data a => a -> DecsQ
genBoilerPlateFromLiteral literal =
  [d| part1 :: Int
      part1 = doPart1 $(liftData literal)
      
      part2 :: Int
      part2 = doPart2 $(liftData literal) |]

genBoilerPlate :: Int -> Int -> DecsQ
genBoilerPlate y d =
  [d| input :: String
      input = $(embedData y d)
      
      part1 :: Int
      part1 = doPart1 $ parse input
      
      part2 :: Int
      part2 = doPart2 $ parse input |]
