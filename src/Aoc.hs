{-# LANGUAGE TemplateHaskell #-}

module Aoc
  ( runAll
  )
where

import           Aoc.TH.Runner

import qualified Aoc.Y2015.D01
import qualified Aoc.Y2015.D02
import qualified Aoc.Y2015.D03
import qualified Aoc.Y2015.D04

$(genRunAll [(2015, [1..4])])
